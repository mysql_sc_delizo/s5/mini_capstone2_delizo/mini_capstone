=======================================
	MYSQL- CULMINATING ACTIVITY
=======================================

-- 1. Return the customerName of the customers who are from the Philippines
		SELECT customers.customerName FROM customers WHERE country = "philippines";


-- 2. Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"
		SELECT customers.contactLastName, customers.contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";


-- 3. Return the product name and MSRP of the product named "The Titanic"
		SELECT products.productName, products.MSRP FROM products WHERE productName = "The Titanic";


-- 4. Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com"
		SELECT employees.lastName, employees.firstName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";


-- 5. Return the names of customers who have no registered state
		SELECT customers.customerName FROM customers;


-- 6. Return the first name, last name, email of the employee whose last name is Patterson and first name is Steve
			SELECT employees.firstName, employees.lastName, employees.email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";


-- 7. Return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000
			SELECT customers.customerName, customers.country, customers.creditLimit FROM customers WHERE country != "USA" AND creditLimit >= 3000;


-- 8. Return the customer numbers of orders whose comments contain the string 'DHL'
			SELECT orders.customerNumber FROM orders WHERE comments like "%DHL%";


-- 9. Return the product lines whose text description mentions the phrase 'state of the art'
			SELECT orders.customerNumber FROM orders WHERE comments like "%DHL%";


-- 10. Return the countries of customers without duplication
			SELECT DISTINCT customers.country FROM customers;


-- 11. Return the statuses of orders without duplication
			SELECT DISTINCT orders.status FROM orders;


-- 12. Return the customer names and countries of customers whose country is USA, France, or Canada
				SELECT customers.customerName FROM customers WHERE country IN ("USA", "France", "Canada");


-- 13. Return the first name, last name, and office's city of employees whose offices are in Tokyo
					SELECT CONCAT(employees.firstName, " ", employees.lastName) FullName, offices.city FROM offices 
							RIGHT JOIN employees ON employees.officeCode = offices.officeCode WHERE city = "Tokyo";


-- 14. Return the customer names of customers who were served by the employee named "Leslie Thompson"
					SELECT customers.customerName FROM customers 
							RIGHT JOIN employees ON employees.employeeNumber = customers.salesRepEmployeeNumber WHERE lastName = "Thompson" AND firstName = "Leslie";


-- 15. Return the product names and customer name of products ordered by "Baane Mini Imports"
-- "La Rochelle Gifts";
			SELECT products.productName, customers.customerName FROM orders
						RIGHT JOIN customers ON customers.customerNumber = orders.customerNumber
						RIGHT JOIN orderdetails ON orderdetails.orderNumber = orders.orderNumber
						RIGHT JOIN products ON products.productCode = orderdetails.productCode  WHERE customers.customerName = "Baane Mini Imports";
				


-- 16. Return the employees' first names, employees' last names, customers' names, and offices' countries of transactions whose customers and offices are in the same country
			SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM customers
				RIGHT JOIN employees ON employees.employeeNumber = customers.salesRepEmployeeNumber
				RIGHT JOIN offices ON offices.country = customers.country ;
						 
	

-- 17. Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000
			SELECT products.productName, products.quantityInStock  FROM products
				RIGHT JOIN productlines ON productlines.productLine =  products.productLine  WHERE products.productLine = "Planes" AND products.quantityInStock < 1000;
				


-- 18. Show the customer's name with a phone number containing "+81".
			SELECT customers.customerName  FROM customers WHERE customers.phone LIKE "%+81%";
				


-- 19. Find all customers from US
			SELECT *  FROM customers WHERE country = "USA";


-- Show the full details of a customer named La Rochelle Gifts.
	SELECT *  FROM customers WHERE customerName = "La Rochelle Gifts";


=======================================
	MYSQL- STRETCH GOALS
=======================================

-- 1. Return the customer names of customers whose customer names don't have 'a' in them
		SELECT customerName  FROM customers WHERE customerName NOT LIKE "%a%" ;


-- 2. Return the last names and first names of employees being supervised by "Anthony Bow"
		SELECT employees.lastName, employees.firstName  FROM employees WHERE reportsTo = 1143 ;

-- 3. Return the product name and MSRP of the product with the highest MSRP
		SELECT productName from products WHERE MSRP = (SELECT MAX(MSRP) FROM products) ;


-- 4. Return the number of customers in the UK
		SELECT COUNT(customerName) from customers WHERE country = "UK" ;


-- 5. Return the number of products per product line
		SELECT COUNT(products.productName), productlines.productLine from products 
						RIGHT JOIN productlines ON products.productLine = productlines.productLine GROUP BY productlines.productLine;


-- 6. Return the number of customers served by every employee
			SELECT COUNT(customers.customerName), employees.firstName, employees.lastName from customers 
						RIGHT JOIN employees ON employees.employeeNumber = customers.salesRepEmployeeNumber  WHERE jobTitle = "Sales Rep" GROUP BY employees.employeeNumber  ORDER BY firstName ASC ;


-- 7. Show the customer's name with the highest credit limit.
	SELECT customerName, creditLimit from customers WHERE creditLimit = (SELECT MAX(creditLimit) FROM customers) ;